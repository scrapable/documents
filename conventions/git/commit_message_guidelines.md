# Scrapable commit convention

Commit messages look like this:
```
type(scope?): Message header

Message body paragraph 1

Message body paragraph 2

...


Message body paragraph n
```

The scope can be omitted if the change affects the whole monorepo,
in that case the parentheses are omitted as well.

If the commit includes a breaking change, the first paragraph of the
body has to start with `BREAKING CHANGE: ` (followed by an uppercase
letter) and describe briefly what's broken. The description has to be
contained within one paragraph.

## Types

Type		| Meaning 												| Release generated
|-|-|-|
__chore__	| Regular/unavoidable tasks that do not change anything	| No release
__feat__	| New features											| Minor bump
__fix__		| Bugfix												| Patch bump
__test__	| Changes/additions to testing suites					| No release
__flow__	| Changes to build/test/CI/deploy workflow/code			| No release (except if triggered automatically,<br>in that case patch bump)
__style__	| Beautification of code, improvements of				| No release code style guidelines. Must not<br>change meaning of code, else fix.
__docs__	| Improvements (of any kind) of documentation			| No release but deploy of docs


## Scopes

 - viscrapable
 - engine
