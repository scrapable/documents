# Git conventions

We drank the monorepo juice.

Branch names don't have to be structured, they should just be
descriptive. Semantics are already handled by commit messages.
(See [commit message guidelines](./commit_message_guidelines.md).)

This repo has to follow no real guidelines, just make it look kinda
like what it already looks like now.
